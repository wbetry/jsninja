var arr = [1, 2, 3, 4, 5]

console.log('Array =>', arr)

var indexOf2 = arr.indexOf(2)

console.log('Índice do valor 2 =>', indexOf2)

var indexOf50IsInvalid = arr.indexOf(50)

console.log('Valor não existe no Array (retorna -1) =>', indexOf50IsInvalid)

var indexOf2InitWithIndex2 = arr.indexOf(2, 2)

console.log('Pesquisa valor a partir do índice 2 =>', indexOf2InitWithIndex2)

// lastIndexOf()

var arr2 = [1, 2, 3, 4, 5, 1]

var lastIndexOf1 = arr2.lastIndexOf(1)

console.log('Última ocorrência do valor 1 no índice ', lastIndexOf1)

var indexOf5InitWithIndex3 = arr2.lastIndexOf(5, 3)

console.log('Procura O último índice que possui o valor 5 a partir do índice 3 =>', indexOf5InitWithIndex3)
