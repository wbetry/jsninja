// Itera sobre o array e retorna true se TODOS os itens respeitarem a condição.

var arr = [1, 2, 3, 4, 5]

var result1 = arr.every(function (item) {
  return item != 10
})

console.log('Array =>', arr)

console.log('Todos os itens são difentes de 10?', result1)

var result2 = arr.every(function (item) {
  return item < 3
})

console.log('Todos os itens são menores que 3?', result2)
