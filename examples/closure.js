function adder (x) {
  return function (y) {
    return x + y
  }
}

var add = adder(5)
console.log(add(2))

console.log(adder(2)(3))
