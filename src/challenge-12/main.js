(function () {
  /*
  Envolva todo o conteúdo desse arquivo em uma IIFE.
  */

  /*
  Crie um objeto chamado `person`, com as propriedades:
      `name`: String
      `lastname`: String
      `age`: Number
  Preencha cada propriedade com os seus dados pessoais, respeitando o tipo
  de valor para cada propriedade.
  */
  var person = {
    name: 'Jon',
    lastname: 'Jovi',
    age: 50
  }
  console.log('Propriedades de "person":')

  /*
  Mostre no console, em um array, todas as propriedades do objeto acima.
  Não use nenhuma estrutura de repetição, nem crie o array manualmente.
  */
  var properties = Object.keys(person)

  console.log(properties)

  /*
  Crie um array vazio chamado `books`.
  */
  var books = []

  /*
  Adicione nesse array 3 objetos, que serão 3 livros. Cada livro deve ter a
  seguintes propriedades:
  `name`: String
  `pages`: Number
  */
  books.push({
    name: 'O milagre da Manhã',
    pages: 200
  })
  books.push({
    name: 'Sapiens',
    pages: 300
  })
  books.push({
    name: 'Rápido e devagar',
    pages: 280
  })

  console.log('\nLista de livros:')

  /*
  Mostre no console todos os livros.
  */
  console.log(books)

  console.log('\nLivro que está sendo removido:')
  /*
  Remova o último livro, e mostre-o no console.
  */
  var lastBook = books.pop()

  console.log(lastBook)

  console.log('\nAgora sobraram somente os livros:')
  /*
  Mostre no console os livros restantes.
  */
  console.log(books)

  /*
  Converta os objetos que ficaram em `books` para strings.
  */
  var objToString = JSON.stringify(books)
  console.log('\nLivros em formato string:')

  /*
  Mostre os livros nesse formato no console:
  */
  console.log(objToString)

  /*
  Converta os livros novamente para objeto.
  */
  var stringToObject = JSON.parse(objToString)
  console.log('\nAgora os livros são objetos novamente:', stringToObject)

  /*
  Mostre no console todas as propriedades e valores de todos os livros,
  no formato abaixo:
      "[PROPRIEDADE]: [VALOR]"
  */
  books.forEach(function (book, index) {
    console.log('Livro', index)
    for (let property in book) {
      console.log(property + ': ' + book[property])
    }
    console.log()
  })

  /*
  Crie um array chamado `myName`. Cada item desse array deve ser uma letra do
  seu nome. Adicione seu nome completo no array.
  */

  var name = 'Jorge'
  var myName = name.split('')

  console.log('\nMeu nome é:')

  /*
  Juntando todos os itens do array, mostre no console seu nome.
  */
  console.log(myName.join(''))

  console.log('\nMeu nome invertido é:')

  /*
  Ainda usando o objeto acima, mostre no console seu nome invertido.
  */
  console.log(myName.reverse().join(''))

  console.log('\nAgora em ordem alfabética:')
  /*
  Mostre todos os itens do array acima, odenados alfabéticamente.
  */
  var sorted = myName.sort()

  console.log(sorted)
})()
