var arr1 = [1, 3, 5, 7, 9]

var arr2 = arr1.slice(1)

console.log('Array original =>', arr1)

console.log('Array a partir do índice 1 =>', arr2)

var arr3 = arr1.slice(0, 2)

console.log('Array com os índices de 0 a 1 (0 a 2, sem mostrar o 2) =>', arr3)

var arr4 = arr1.slice(1, 2)

console.log('Array com índice 1 (1 a 2 sem mostrar o 2) =>', arr4)

var arr5 = arr1.slice(3, 3)

console.log('Array vazio (3 a 3 sem mostrar o 3) =>', arr5)

var arr6 = arr1.slice(2, 1)

console.log('Array vazio (Slice inválido) =>', arr6)

var arr7 = arr1.slice(-2)

console.log('Array com os dois últimos itens', arr7)
