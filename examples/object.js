// Mutáveis

var object = {
  prop1: 1,
  prop2: true,
  prop3: {}
}

console.log('Estado 1 =>', object)

object.prop1 = []

console.log('Estado 2 =>', object)

delete object.prop3

console.log('Estado 3 =>', object)

// Manipulados por referência

copy = object

console.log('Original =>', object)
console.log('Copia =>', copy)

console.log('É identico?', copy === object)

copy.prop1 = 'Sei lá'

console.log('Original =>', object)
console.log('Copia =>', copy)
