var arr = [1, 2, 3]

console.log('Array inicial =>', arr)

arr.unshift(4)
arr.unshift(5)
arr.unshift([6, 7, 8])

console.log('Adicionando elementos ao inicio do Array =>', arr)

var last = arr.shift()

console.log('O item removido do início do Array =>', last)
console.log('Array final =>', arr)
