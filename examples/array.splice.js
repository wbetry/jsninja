var arr1 = [1, 2, 3, 4, 5]

var arr2 = arr1.splice(1)

console.log('Array original modificado =>', arr1)
console.log('Array resultante do slice =>', arr2)

var arr3 = [1, 2, 3, 4, 5]

var arr4 = arr3.splice(-2)

console.log('Array original modificado =>', arr3)
console.log('Array resultante do slice =>', arr4)

var arr5 = [1, 2, 3, 4, 5, 6, 7, 8]

// O segundo argumento indica quantos elementos serão adicionados ao slice
var arr6 = arr5.splice(1, 3)

console.log('Array original modificado =>', arr5)
console.log('Array resultante do slice (3 itens a partir do índice 1) =>', arr6)

arr5.splice(1, 0, 'potato')

console.log('Array modificado adicionando elemento no índice 1 =>', arr5)

// A partir do terceiro argumento estarão os itens que serão inseridos
arr5.splice(1, 1, 2, 3, 4)

console.log('Removendo elemento com índice 1 e adicionando os elementos 2, 3 e 4 a partir do índice 1=>', arr5)
