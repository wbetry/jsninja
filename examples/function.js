var car = {
  color: 'Blue'
}

function getCarColor (myCar) {
  return myCar.color
}

console.log(getCarColor(car))

function showOtherFunction (func) {
  return func()
}

showOtherFunction(function () {
  return 'Hello'
})

function myFunction () {
  var n1 = 1
  var n2 = 2
  function sum () {
    return n1 + n2
  }
  return sum()
}

myFunction()
