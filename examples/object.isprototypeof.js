console.log('Implementando herança de objetos em javascript')

var obj1 = {
  x: 1,
  y: 2,
  z: 3
}

var obj2 = Object.create(obj1)

var obj3 = Object.create(obj2)

console.log('obj1 é prototipo de obj2 que é prototipo de obj3')

console.log('obj1 é prototipo de obj2?', obj1.isPrototypeOf(obj2))
console.log('obj1 é prototipo de obj3?', obj1.isPrototypeOf(obj3))

console.log('obj2 é prototipo de obj1?', obj2.isPrototypeOf(obj1))
console.log('obj2 é prototipo de obj3?', obj2.isPrototypeOf(obj3))

console.log('obj3 é prototipo de obj1?', obj3.isPrototypeOf(obj1))
console.log('obj3 é prototipo de obj2?', obj3.isPrototypeOf(obj2))
