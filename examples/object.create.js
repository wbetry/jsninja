// Criar ojeto no formato literal

var obj1 = {
  x: 1,
  y: 7.2
}

console.log('Objeto literal =>', obj1)

// Criar objeto com construtor

var obj2 = new Object()

console.log('Objeto criado com construtor =>', obj2)

// Criar objeto com object.create

var obj3 = Object.create(obj1)

console.log('Objeto criado a partir de objec.create "herdando" de obj1 =>', obj3)

console.log('Propriedade x =>', obj3.x)
console.log('Propriedade y =>', obj3.y)

obj1.x = 5

console.log('Alterando x em obj1 temos:', 'obj1.x =>', obj1.x, 'obj3.x =>', obj3.x)

console.log('Exibindo ambos', 'obj1 =>', obj1, 'obj3 =>', obj3)

obj3.x = 10

console.log('Alterando x em obj3 temos:', 'obj1.x =>', obj1.x, 'obj3.x =>', obj3.x)

console.log('Exibindo ambos', 'obj1 =>', obj1, 'obj3 =>', obj3)

obj1.x = 7

console.log('Alterando novamente x em obj1 temos:', 'obj1.x =>', obj1.x, 'obj3.x =>', obj3.x)

console.log('Exibindo ambos', 'obj1 =>', obj1, 'obj3 =>', obj3)

obj1.z = 8

console.log('Adicionando propriedade z obj1')

console.log('Exibindo ambos', 'obj1 =>', obj1, 'obj3 =>', obj3, 'obj3.z =>', obj3.z)
