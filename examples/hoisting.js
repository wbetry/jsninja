function myFunc () {
  console.log('Antes de Declarar', sum())
  function sum () {
    return 1 + 2
  }
  console.log('Depois de Declarar', sum())
}

myFunc()

console.log('\n\n')

function myFunc2 () {
  // console.log('Antes de Declarar', sum()) // Não tem hoisting
  var sum = function () {
    return 1 + 2
  }
  console.log('Depois de Declarar', sum())
}

myFunc2()

console.log('\n\n')

function myFunc3 () {
  console.log('Antes de Declarar', variable1)
  var variable1 = 1
  console.log('Depois de Declarar', variable1)
  // console.log('Depois de Declarar', variable2) // variable2 not defined
}

myFunc3()
